;;------------------------------------------------------------------------------
;; To the extent possible under law, the author(s) have dedicated all copyright
;; and related and neighboring rights to this software to the public domain
;; worldwide.  This software is distributed without any warranty.
;;
;; You should have received a copy of the CC0 Public Domain Dedication along
;; with this software.  If not, see
;; <http://creativecommons.org/publicdomain/zero/1.0/>.
;;------------------------------------------------------------------------------

(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-install 'use-package))
(require 'use-package)
(setq-default use-package-always-ensure t)

(unless package-archive-contents
  (package-refresh-contents))

;;------------------------------------------------------------------------------

(setq-default
 cursor-type 'bar
 default-frame-alist '((width . 135) (height . 40))
 truncate-lines t
 fill-column 80
 backup-directory-alist `(("." . "~/.emacs.d/saves/"))
 auto-save-file-name-transforms `((".*" "~/.emacs.d/saves/" t))
 custom-file "~/.emacs.d/custom.el"
 initial-scratch-message ""
 initial-major-mode 'markdown-mode
 inhibit-startup-screen t
 indent-tabs-mode nil
 custom-safe-themes t)

;; Disable default Emacs elements
(tool-bar-mode 0)
(toggle-scroll-bar 0)
(menu-bar-mode 0)
(line-number-mode 0)

(global-hl-line-mode 1) ;; Highlight current line
(show-paren-mode 1)     ;; Highlight matching brackets
(electric-pair-mode 1)  ;; Bracket autocomplete

(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

;; Keybindings
(global-set-key (kbd "C-u") 'undo)
(global-set-key (kbd "C-j") 'indent-region)
(global-set-key (kbd "C-+") 'text-scale-increase)
(global-set-key (kbd "C--") 'text-scale-decrease)

;; Flash mode line on alarm
(defun flash-mode-line ()
  (invert-face 'mode-line)
  (run-with-timer 0.1 nil #'invert-face 'mode-line))

(setq-default ring-bell-function 'flash-mode-line)

(defun prog-minor-modes ()
  (setq show-trailing-whitespace t)
  (display-fill-column-indicator-mode)
  (display-line-numbers-mode))

(add-hook 'prog-mode-hook 'prog-minor-modes)
(add-hook 'latex-mode-hook 'prog-minor-modes)
(add-hook 'markdown-mode-hook 'prog-minor-modes)
(add-hook 'nxml-mode-hook 'prog-minor-modes)
(add-hook 'yaml-ts-mode-hook 'prog-minor-modes)

(load custom-file t)

(defun find-init ()
  (interactive)
  (find-file "~/.emacs.d/init.el"))

(defconst local-file "~/.emacs.d/local.el")
(load local-file t)
(defun find-local ()
  (interactive)
  (find-file local-file))

;;------------------------------------------------------------------------------

(use-package doom-themes)
(use-package kaolin-themes)
(use-package spacemacs-theme :defer t)

(defconst themes [doom-acario-light
                  doom-dark+
                  doom-dracula
                  doom-one
                  kaolin-light
                  kaolin-light-alt-bg
                  doom-opera-light
                  spacemacs-dark
                  spacemacs-light])
(defvar active-theme 2)

(defun cycle-theme ()
  (interactive)
  (setq active-theme (+ active-theme 1))
  (when (>= active-theme (length themes))
    (setq active-theme 0))
  (load-theme (aref themes active-theme)))

(load-theme (aref themes active-theme))

(use-package solaire-mode
  :hook (treemacs-mode . solaire-mode)
  :config (setq-default solaire-mode-auto-swap-bg nil))

(use-package doom-modeline
  :config
  (setq-default
   doom-modeline-minor-modes t
   doom-modeline-height 25))
(doom-modeline-mode 1)

(use-package centaur-tabs
  :config
  (setq-default
   centaur-tabs-enable-key-bindings t
   centaur-tabs-style "bar"
   centaur-tabs-height 28
   centaur-tabs-set-icons t
   x-underline-at-descent-line t
   centaur-tabs-set-bar 'under)
  :bind
   ("C-c C-<left>" . centaur-tabs-backward)
   ("C-c C-<right>" . centaur-tabs-forward)
   ("C-c C-<up>" . centaur-tabs-forward-group)
   ("C-c C-<down>" . centaur-tabs-backward-group))
(centaur-tabs-mode 1)

(use-package all-the-icons)
(use-package all-the-icons-completion
  :config (all-the-icons-completion-mode))
(use-package all-the-icons-dired
  :hook (dired-mode . all-the-icons-dired-mode))

(defun install-fonts ()
  (interactive)
  (all-the-icons-install-fonts)
  (nerd-icons-install-fonts))

(use-package treemacs)
(use-package treemacs-all-the-icons
  :after (treemacs)
  :config (treemacs-load-theme "all-the-icons"))

;;------------------------------------------------------------------------------

(use-package string-inflection)

(use-package yasnippet
  :config
  (setq-default yas-snippet-dirs '("~/.emacs.d/snippets"))
  (yas-global-mode 1))

(use-package company
  :hook (emacs-lisp-mode . company-mode))

(use-package flycheck)
(use-package flycheck-inline
  :hook (flycheck-mode . flycheck-inline-mode))

(use-package lsp-ui)
(use-package lsp-mode
  :init
  (defun ts-hook ()
    (lsp)
    ;; Because setting lsp-eslint-auto-fix-on-save doesn't do anything?
    (add-hook 'before-save-hook 'lsp-eslint-apply-all-fixes))
  :config
  (setq-default lsp-lens-enable nil)
  ;; I couldn't find a cleaner way to check if the eslint server is installed
  (unless (file-directory-p "~/.emacs.d/.cache/lsp/eslint")
    (lsp-install-server nil 'eslint))
  :hook
  (c-ts-mode . lsp)
  (c++-ts-mode . lsp)
  (js-ts-mode . lsp)
  (latex-mode . lsp)
  (python-ts-mode . lsp)
  (typescript-ts-base-mode . ts-hook))

(use-package treesit-auto
  :custom (treesit-auto-install 'prompt)
  :config
  (treesit-auto-add-to-auto-mode-alist 'all)
  (global-treesit-auto-mode))

(use-package blueprint-ts-mode)

(use-package meson-mode
  :hook (meson-mode . company-mode))

(use-package yaml-mode)

(use-package ccls)
(setq-default c-default-style "gnu")
(c-set-offset 'brace-list-open -2)
(font-lock-add-keywords
 'c-mode
 '(("g_assert_[a-z]+\\_>" . font-lock-keyword-face)
   ("g_test_expect_[a-z]+\\_>" . font-lock-keyword-face)
   ("g_test_assert_[a-z\|_]+\\_>" . font-lock-keyword-face)))

(use-package tide
  :hook
  (tsx-ts-mode . tide-setup)
  (typescript-ts-mode . tide-setup))
(setq-default js-indent-level 2)
(add-to-list 'auto-mode-alist '("\\.cjs" . javascript-mode))

(use-package web-mode
  :custom
  (web-mode-markup-indent-offset 2)
  (web-mode-css-indent-offset 2)
  (web-mode-code-indent-offset 2)
  (web-mode-auto-quote-style 2)
  :mode ("\\.ejs" . web-mode))

(use-package svelte-mode)

(use-package geiser)
(use-package geiser-guile)
